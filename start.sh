#!/bin/bash
cd /apps/iotgw-teltonika
lib="iotgw-teltonika"
if [ ! -f pids/app$1.pid ]
     then
        ./$lib $1 > /dev/null 2>&1 & echo $! > pids/app$1.pid
     echo "started:$1"
     else echo "udah jalan ? "
fi
