# GO TELTONIKA
TCP server teltonika codec8


## How to build binary
```
./build.sh
```

## How to run
Run use configuration file based on environment.
default ENV=local, configuration will use config.local.json

### Command
```
export ENV={local/dev/prod} && ./go-teltonika 
```

## Configuration
```
{
  "apps": {
    "name": "go-teltonika",
    "address": "0.0.0.0",
    "port": 1883,
    "deviceType":["fmb130","mtb100"]
  },
  "data":[
    {"id":"72","name":"temp", "divider":10.0},
    {"id":"9","name":"sensor", "type":"bool", "min":0, "max":4500},
    {"id":"113","name":"battery"},
    {"id":"66","name":"external_power"},
    {"id":"67","name":"internal_power"}
  ],
  "logger": {
    "fileTdrLocation": "logs/tdr.log",
    "fileSyslogLocation": "logs/syslog.log",
    "fileMaxAge": 30,
    "stdout": true
  },
  "http": {
    "enable": true,
    "method": "POST",
    "timeout": 10000,
    "url": "https://connectinc.telkomsel.com/tsel-iot/api/thing/push-data",
    "body":{
      "tokenKey": "[token]",
      "jsonData": "{ \"latlong\" : \"[longitude],[latitude]\",\"temp\":[data.72],\"sensor\":[data.9],\"battery\":[data.103],\"external_power\":[data.66],\"internal_power\":[data.67]}"
    }
  },
  "dbDevice": {
    "enable": false,
    "host": "localhost",
    "username": "root",
    "password": "Keysha909",
    "database": "iotgw_api",
    "port": 3306,
    "maxIdleConn": 10,
    "maxOpenConn": 30,
    "maxLifeTime": 900
  },
  "mysql": {
      "enable": false,
      "host": "localhost",
      "username": "root",
      "password": "Keysha909",
      "database": "connectinc_iot",
      "port": 3306,
      "maxIdleConn": 10,
      "maxOpenConn": 30,
      "maxLifeTime": 900
  },
  "kafka": {
    "enable": false,
    "clientId": "go-teltonika",
    "brokers": ["localhost:9092"],
    "topics": "teltonika"
  }
}

  
```
