package main

import (
	"bytes"
	"crypto/tls"
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/Shopify/sarama"
	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/cast"
	"gitlab.com/itbis/iotgw-teltonika/codec8"
	"gitlab.com/itbis/iotgw-teltonika/config"
	"gitlab.com/itbis/iotgw-teltonika/logger"
	"go.uber.org/zap"
)

type Logger struct {
	tdr    *zap.Logger
	syslog *zap.Logger
}

func SetupLogger(conf *config.Default) *Logger {

	loggerTdr := logger.NewZapLogger(conf.Logger.Stdout, conf.Logger.Filetdrlocation, time.Duration(conf.Logger.Filemaxage))
	loggerSyslog := logger.NewZapLogger(conf.Logger.Stdout, conf.Logger.Filesysloglocation, time.Duration(conf.Logger.Filemaxage))
	return &Logger{tdr: loggerTdr, syslog: loggerSyslog}

}

var (
	onceDb       sync.Once
	db           *sql.DB
	onceDbDevice sync.Once
	dbDevice     *sql.DB
)

func NewDbMySQL(conf *config.Default, logger *Logger) *sql.DB {
	onceDb.Do(func() {
		dbDriver := "mysql"
		dbUser := conf.Mysql.Username
		dbPass := conf.Mysql.Password
		dbName := conf.Mysql.Database
		dbHost := conf.Mysql.Host
		dbConn, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@tcp("+dbHost+")/"+dbName)
		if err != nil {
			logger.syslog.Error("NewDbMySQL", zap.Any("error", err))
			panic(err)
		}
		db = dbConn
	})
	return db
}

func NewDbDevice(conf *config.Default, logger *Logger) *sql.DB {
	onceDbDevice.Do(func() {
		dbDriver := "mysql"
		dbUser := conf.DbDevice.Username
		dbPass := conf.DbDevice.Password
		dbName := conf.DbDevice.Database
		dbHost := conf.DbDevice.Host
		dbConn, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@tcp("+dbHost+")/"+dbName)
		if err != nil {
			logger.syslog.Error("NewDbDevice", zap.Any("error", err))
			panic(err)
		}
		dbDevice = dbConn
	})
	return dbDevice
}

func SubmitMySQLCodec8(conf *config.Default, message *codec8.Message, logger *Logger) error {
	if message == nil || len(message.AvlData) == 0 {
		return errors.New("Not found message")
	}

	var err error
	db = NewDbMySQL(conf, logger)
	//defer db.Close()

	sql, err := db.Prepare("INSERT INTO teltonika(imei,timestamp,latitude,longitude,priority,altitude,angle,satelites,speed) VALUES(?,?,?,?,?,?,?,?,?)")
	if err != nil {
		logger.syslog.Error("SubmitMySQLCodec8", zap.Any("error", err))
		return err
	}

	timestamp := message.AvlData[0].Timestamp
	priority := message.AvlData[0].Priority
	latitude := message.AvlData[0].Latitude
	longitude := message.AvlData[0].Longitude
	altitude := message.AvlData[0].Altitude
	angle := message.AvlData[0].Angle
	satelites := message.AvlData[0].Satelites
	speed := message.AvlData[0].Speed

	_, err = sql.Exec(message.Imei, timestamp, latitude, longitude, priority, altitude, angle, satelites, speed)
	if err != nil {
		logger.syslog.Error("SubmitMySQLCodec8", zap.Any("error", err))

	}
	return err

}

func HitAPI(url string, method string, timeout int, headers map[string]string, payload string) (body []byte, err error) {

	req, err := http.NewRequest(method, url, bytes.NewBuffer([]byte(payload)))
	if err != nil {
		return
	}

	for k, v := range headers {
		req.Header.Set(k, v)
	}
	to := time.Duration(time.Duration(timeout) * time.Second)
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{
		Timeout:   to,
		Transport: tr,
	}

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	return
}

type RequestConnectinct struct {
	Imei      string `json:"imei"`
	Timestamp string `json:"timestamp"`
	TokenKey  string `json:"token_key"`
	JsonData  string `json:"json_data"`
}

func SubmitHTTPCodec8(conf *config.Default, message *codec8.Message, logger *Logger) error {
	if message == nil || len(message.AvlData) == 0 {
		return errors.New("Not found message")
	}
	var err error
	url := conf.HTTP.URL
	method := conf.HTTP.Method
	timeout := conf.HTTP.Timeout

	headers := map[string]string{}
	headers["Content-Type"] = "application/json"
	var body []byte

	imei := message.Imei
	timestamp := message.AvlData[0].Timestamp
	latitude := cast.ToString(message.AvlData[0].Latitude)
	longitude := cast.ToString(message.AvlData[0].Longitude)

	//post method
	tokenKey := conf.HTTP.Body.TokenKey

	jsonData := "{ \"latlong\" : \"" + latitude + "," + longitude + "\""

	for _, v := range conf.Data {
		for _, x := range message.AvlData[0].IOElement {
			if v.ID == cast.ToString(x.ID) {
				//jsonData = strings.Replace(jsonData, "[data."+v.ID+"]", cast.ToString(x.Value), -1)
				jsonData += ",\"" + v.Name + "\":" + cast.ToString(x.Value)

				break
			}
		}
	}
	jsonData += "}"

	/*for _, v := range message.AvlData[0].IOElement {
		if v.Value != nil {
			jsonData += "{ \"latlong\" : \"" + longitude + "," + latitude + "\""
		}
	}
	*/
	//jsonData = strings.Replace(jsonData, "[longitude]", longitude, -1)
	//jsonData = strings.Replace(jsonData, "[latitude]", latitude, -1)
	/*endData:
		"{ \"latlong\" : \"[longitude],[latitude]\"}"
	    },\"temp\":[data.72],\"sensor\":[data.9],\"battery\":[data.103],\"external_power\":[data.66],\"internal_power\":[data.67]
	*/
	/*for _, v := range conf.Data {
		for _, x := range message.AvlData[0].IOElement {
			if v.ID == cast.ToString(x.ID) {
				jsonData = strings.Replace(jsonData, "[data."+v.ID+"]", cast.ToString(x.Value), -1)
				break
			}
		}
	}
	*/

	reqConnectinct := &RequestConnectinct{
		Imei:      imei,
		Timestamp: timestamp,
		TokenKey:  tokenKey,
		JsonData:  jsonData,
	}
	body, _ = json.Marshal(reqConnectinct)

	fmt.Printf("test %s", body)
	logger.syslog.Info("SubmitHTTPCodec8", zap.Any("http", url), zap.String("body", string(body)))
	response, err := HitAPI(url, method, timeout, headers, string(body))

	if err != nil {
		logger.syslog.Error("SubmitHTTPCodec8", zap.Any("error", err))

	}
	logger.syslog.Info("SubmitHTTPCodec8", zap.Any("response", string(response)))
	return err

}

var (
	onceKafka sync.Once
	kafka     sarama.SyncProducer
)

func NewKafkaProducer(conf *config.Default, logger *Logger) sarama.SyncProducer {
	onceKafka.Do(func() {
		kafkaConfig := sarama.NewConfig()
		kafkaConfig.ClientID = conf.Kafka.Clientid
		kafkaConfig.Producer.Return.Errors = true
		kafkaConfig.Producer.Return.Successes = true
		kafkaBrokers := conf.Kafka.Brokers
		kafkaProducer, err := sarama.NewSyncProducer(kafkaBrokers, kafkaConfig)
		if err != nil {
			//fmt.Println("SetupKafka-error", err)
			logger.syslog.Error("NewKafkaProducer", zap.Any("error", err))
			panic(err)
		}
		kafka = kafkaProducer

	})
	return kafka
}

func SubmitKafkaCodec8(conf *config.Default, message *codec8.Message, logger *Logger) error {

	if message == nil || len(message.AvlData) == 0 {
		return errors.New("Not found message")
	}

	kafkaProducer := NewKafkaProducer(conf, logger)
	topic := conf.Kafka.Topics

	reqByte, err := json.Marshal(message)
	if err != nil {
		return err
	}
	msg := string(reqByte)
	kafkaMsg := &sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.StringEncoder(msg),
	}
	partition, offset, err := kafkaProducer.SendMessage(kafkaMsg)
	if err != nil {
		logger.syslog.Error("SubmitKafkaCodec8", zap.Any("error", err))
		return err
	}
	logger.syslog.Debug("SubmitKafkaCodec8", zap.Any("partition", partition), zap.Any("offset", offset))

	return err
}

func ValidateDevice(conf *config.Default, imei string, logger *Logger) (string, error) {

	var token string
	var err error
	dbDevice = NewDbDevice(conf, logger)
	//defer dbDevice.Close()

	arrayDeviceType := "'" + strings.Join(conf.Apps.DeviceType[:], "','") + "'"
	query := "SELECT token from device where imei='" + imei + "' and is_active=1 and device_type in (" + arrayDeviceType + ")"
	sql, err := dbDevice.Prepare(query)
	if err != nil {
		logger.syslog.Error("Error SQL validate imei", zap.Any("sql", query), zap.Any("error", err))
		return token, err
	}

	logger.syslog.Debug("SQL", zap.Any("sql", query))
	rows, err := sql.Query()
	if err != nil {
		logger.syslog.Error("Error SQL validate imei", zap.Any("sql", query), zap.Any("error", err))
		return token, err
	}

	for rows.Next() {
		if err := rows.Scan(&token); err != nil {
			logger.syslog.Error("Error SQL validate imei", zap.Any("sql", query), zap.Any("error", err))
			return token, err
		}
		logger.syslog.Debug("Token", zap.Any("token", token))

	}

	return token, err

}
