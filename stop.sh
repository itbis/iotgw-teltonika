#!/bin/bash
cd /apps/iotgw-teltonika
if [ -f pids/app$1.pid ]; then
        PID=`cat pids/app$1.pid`
        echo "kill pid $PID"
        kill -9 $PID
        else
        echo "no_pid found"
fi
rm -f pids/app$1.pid
