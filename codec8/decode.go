package codec8

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/spf13/cast"
	"gitlab.com/itbis/iotgw-teltonika/config"
	"gitlab.com/itbis/iotgw-teltonika/teltonika"
)

// Teltonika sample TCP packet:
// device send imei: 00 0F 33 35 32 38 34 38 30 32 33 36 30 33 32 33 32
// server response : 01 if OK, 00 if not allow
// device send 00000000 ;4bytes 00, begin TCP packet
//             000003B9 ;AVLpacket length int, 4 bytes
//             08  ;CodecID FM4XXX, start of AVLpkt
//             1E  ;NumOfRecords, max 30=1E, max 1024Bytes?
//                           ;data of 1st record
//                           ;data of 2nd record
//                           ; ...
//                           ;data of last record
//             1E  ;NumOfRecords, max 30=1E
//             0000abcd ;4 bytes CRC
// server response actuall received NumOfRecords 1E

//https://wiki.teltonika-gps.com/view/Codec#Codec_8

type Payload struct {
	data   string
	offset int
}

type Message struct {
	Imei         string    `json:"imei"`
	Preamble     int64     `json:"preamble,omitempty"`
	Length       int64     `json:"length"`
	CodecID      int8      `json:"codecId"`
	NumberOfData int8      `json:"numberOfData"`
	AvlData      []AvlData `data:"data"`
}
type AvlData struct {
	Timestamp string      `json:"timestamp"`
	Priority  int8        `json:"priority"`
	Longitude float64     `json:"long"`
	Latitude  float64     `json:"lat"`
	Altitude  int16       `json:"altitude"`
	Angle     int16       `json:"angle"`
	Satelites int8        `json:"satelites"`
	Speed     int16       `json:"speed"`
	EventIOID int8        `json:"eventIOID"`
	NumOfIO   int8        `json:"numOfIO"`
	IOElement []IOElement `json:"ioElement"`
}
type IOElement struct {
	ID    int8        `json:"id"`
	Value interface{} `json:"value"`
	Desc  string      `json:"desc"`
}

type Element struct {
	No              string `json:"No"`
	Propertyname    string `json:"PropertyName"`
	Bytes           string `json:"Bytes"`
	Type            string `json:"Type"`
	Min             string `json:"Min"`
	Max             string `json:"Max"`
	Multiplier      string `json:"Multiplier"`
	Units           string `json:"Units"`
	Description     string `json:"Description"`
	Hwsupport       string `json:"HWSupport"`
	ParametrGroup   string `json:"Parametr Group"`
	Finalconversion string `json:"FinalConversion"`
}

const PRECISION = 10000000.0

var ElementDesc map[string]Element

func init() {
	var m map[string]interface{}
	err := json.Unmarshal([]byte(teltonika.FMBXY), &m)
	if err != nil {
		log.Fatal(err)
	}
	ElementDesc = make(map[string]Element)
	for k, v := range m {
		jsonString, _ := json.Marshal(v)
		value := &Element{}
		err = json.Unmarshal([]byte(jsonString), &value)
		ElementDesc[k] = *value
	}
}

func (p *Payload) readNext(length int) string {
	length = length * 2
	if len(p.data) < p.offset+length {
		return ""
	}

	data := p.data[p.offset : p.offset+length]
	p.offset = p.offset + length
	return "0x" + data
}

func New() *Message {
	return &Message{}
}
func NewWithImei(imei string) Message {
	return Message{Imei: imei}
}

func (m *Message) Decode(data string, element *[]config.Data) error {

	p := &Payload{}
	p.data = data
	p.offset = 0

	m.Preamble = cast.ToInt64(p.readNext(4))
	m.Length = cast.ToInt64(p.readNext(4))
	m.CodecID = cast.ToInt8(p.readNext(1))
	m.NumberOfData = cast.ToInt8(p.readNext(1))
	//fmt.Println("Message", m)
	for i := 0; i < 1; i++ {
		//for i := 0; i < int(m.NumberOfData); i++ {
		data := &AvlData{}
		timestamp := p.readNext(8)
		priority := p.readNext(1)
		longitude := p.readNext(4)
		latitude := p.readNext(4)
		altitude := p.readNext(2)
		angle := p.readNext(2)
		satelites := p.readNext(1)
		speed := p.readNext(2)
		EventIOID := p.readNext(1)
		NumOfIO := p.readNext(1)

		ts := time.Unix(cast.ToInt64(timestamp)/1000, 0) //gives unix time stamp in utc
		data.Timestamp = cast.ToString(ts.Format(time.RFC3339))
		data.Priority = cast.ToInt8(priority)

		longitudeTemp := cast.ToFloat64(cast.ToInt32(longitude)) / PRECISION
		sign := 1
		if hexToBin(longitude)[0:1] != "0" {
			sign = -1
			longitudeTemp = longitudeTemp * float64(sign)
		}
		data.Longitude = longitudeTemp

		latitudeTemp := cast.ToFloat64(cast.ToInt32(latitude)) / PRECISION
		signLat := 1
		if hexToBin(latitude)[0:1] != "0" {
			signLat = -1
			latitudeTemp = latitudeTemp * float64(signLat)
		}
		data.Latitude = latitudeTemp
		data.Altitude = cast.ToInt16(altitude)
		data.Angle = cast.ToInt16(angle)
		data.Satelites = cast.ToInt8(satelites)
		data.Speed = cast.ToInt16(speed)
		data.EventIOID = cast.ToInt8(EventIOID)
		data.NumOfIO = cast.ToInt8(NumOfIO)

		iOElement := &IOElement{}
		numIO_1 := cast.ToInt8(p.readNext(1))
		//fmt.Println("dataIO_1", numIO_1)
		for k := 0; k < int(numIO_1); k++ {
			iOElement.ID = cast.ToInt8(p.readNext(1))
			iOElement.Value = cast.ToInt8(p.readNext(1))
			exist, name, value := isCapture(cast.ToString(iOElement.ID), cast.ToFloat64(iOElement.Value), element)
			if exist {
				iOElement.Desc = name
				iOElement.Value = value
				data.IOElement = append(data.IOElement, *iOElement)
			}

			//iOElement.Desc = ElementDesc[cast.ToString(iOElement.ID)].Description
			//data.IOElement = append(data.IOElement, *iOElement)

		}
		numIO_2 := cast.ToInt8(p.readNext(1))
		//fmt.Println("dataIO_2", numIO_2)
		for k := 0; k < int(numIO_2); k++ {
			iOElement.ID = cast.ToInt8(p.readNext(1))
			iOElement.Value = cast.ToInt16(p.readNext(2))
			exist, name, value := isCapture(cast.ToString(iOElement.ID), cast.ToFloat64(iOElement.Value), element)
			if exist {
				iOElement.Desc = name
				iOElement.Value = value
				data.IOElement = append(data.IOElement, *iOElement)
			}

			//iOElement.Desc = ElementDesc[cast.ToString(iOElement.ID)].Description
			//data.IOElement = append(data.IOElement, *iOElement)

		}

		numIO_4 := cast.ToInt8(p.readNext(1))
		//fmt.Println("dataIO_4", numIO_4)
		for k := 0; k < int(numIO_4); k++ {
			iOElement.ID = cast.ToInt8(p.readNext(1))
			iOElement.Value = cast.ToInt32(p.readNext(4))

			exist, name, value := isCapture(cast.ToString(iOElement.ID), cast.ToFloat64(iOElement.Value), element)
			if exist {
				iOElement.Desc = name
				iOElement.Value = value
				data.IOElement = append(data.IOElement, *iOElement)
			}
			//iOElement.Desc = ElementDesc[cast.ToString(iOElement.ID)].Description
			//data.IOElement = append(data.IOElement, *iOElement)

		}
		numIO_8 := cast.ToInt8(p.readNext(1))
		//fmt.Println("dataIO_8", numIO_8)
		for k := 0; k < int(numIO_8); k++ {
			iOElement.ID = cast.ToInt8(p.readNext(1))
			iOElement.Value = cast.ToInt64(p.readNext(8))
			exist, name, value := isCapture(cast.ToString(iOElement.ID), cast.ToFloat64(iOElement.Value), element)
			if exist {
				iOElement.Desc = name
				iOElement.Value = value
				data.IOElement = append(data.IOElement, *iOElement)
			}

			//iOElement.Desc = ElementDesc[cast.ToString(iOElement.ID)].Description
			//data.IOElement = append(data.IOElement, *iOElement)

		}
		m.AvlData = append(m.AvlData, *data)
	}

	dataPrint, err := json.Marshal(m)
	if err != nil {
		fmt.Println("Error while marshal json", err)
	}
	fmt.Printf("%s\n", dataPrint)

	return nil

}
func hexToBin(hex string) string {
	ui, _ := strconv.ParseUint(hex, 16, 64)
	return fmt.Sprintf("%016b", ui)
}

func isCapture(id string, value float64, element *[]config.Data) (exist bool, name string, valueOutput interface{}) {
	exist = false
	name = ""
	valueOutput = value
	for _, v := range *element {
		if id == v.ID {
			exist = true
			name = v.Name
			if v.Divider > 0 {
				valueOutput = value / float64(v.Divider)
			}
			if v.Type == "bool" {
				if value >= v.Min && value <= v.Max {
					valueOutput = true
				} else {
					valueOutput = false
				}
			}
			break
		}
	}
	return exist, name, valueOutput
}
