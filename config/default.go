package config

type Default struct {
	Apps     Apps     `json:"apps"`
	Data     []Data   `json:"data"`
	Logger   Logger   `json:"logger"`
	DbDevice DbDevice `json:"dbDevice"`
	Mysql    Mysql    `json:"mysql"`
	HTTP     HTTP     `json:"http"`
	Kafka    Kafka    `json:"kafka"`
}
type Apps struct {
	Name       string   `json:"name"`
	Address    string   `json:"address"`
	Port       string   `json:"port"`
	DeviceType []string `json:"deviceType"`
}
type Data struct {
	ID      string  `json:"id"`
	Name    string  `json:"name"`
	Divider float64 `json:"divider,omitempty"`
	Type    string  `json:"type,omitempty"`
	Min     float64 `json:"min,omitempty"`
	Max     float64 `json:"max,omitempty"`
}
type Logger struct {
	Filetdrlocation    string `json:"fileTdrLocation"`
	Filesysloglocation string `json:"fileSyslogLocation"`
	Filemaxage         int    `json:"fileMaxAge"`
	Stdout             bool   `json:"stdout"`
}
type DbDevice struct {
	Enable      bool   `json:"enable"`
	Host        string `json:"host"`
	Username    string `json:"username"`
	Password    string `json:"password"`
	Database    string `json:"database"`
	Port        int    `json:"port"`
	Maxidleconn int    `json:"maxIdleConn"`
	Maxopenconn int    `json:"maxOpenConn"`
	Maxlifetime int    `json:"maxLifeTime"`
}
type Mysql struct {
	Enable      bool   `json:"enable"`
	Host        string `json:"host"`
	Username    string `json:"username"`
	Password    string `json:"password"`
	Database    string `json:"database"`
	Port        int    `json:"port"`
	Maxidleconn int    `json:"maxIdleConn"`
	Maxopenconn int    `json:"maxOpenConn"`
	Maxlifetime int    `json:"maxLifeTime"`
}
type Body struct {
	TokenKey string `json:"tokenKey"`
	JSONData string `json:"jsonData"`
}
type HTTP struct {
	Enable  bool   `json:"enable"`
	Method  string `json:"method"`
	Timeout int    `json:"timeout"`
	URL     string `json:"url"`
	Body    Body   `json:"body"`
}
type Kafka struct {
	Enable   bool     `json:"enable"`
	Clientid string   `json:"clientId"`
	Brokers  []string `json:"brokers"`
	Topics   string   `json:"topics"`
}
