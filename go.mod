module gitlab.com/itbis/iotgw-teltonika

go 1.15

require (
	github.com/Shopify/sarama v1.28.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/lestrrat-go/file-rotatelogs v2.4.0+incompatible
	github.com/lestrrat-go/strftime v1.0.4 // indirect
	github.com/spf13/cast v1.3.1
	github.com/spf13/viper v1.7.1
	go.uber.org/zap v1.16.0
)
