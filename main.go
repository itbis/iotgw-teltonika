package main

import (
	"bufio"
	"fmt"
	"net"
	"os"

	"github.com/spf13/viper"
	"gitlab.com/itbis/iotgw-teltonika/codec8"
	"gitlab.com/itbis/iotgw-teltonika/config"
	"go.uber.org/zap"
)

var token string

func main() {

	//setup config
	env := os.Getenv("ENV")
	if env == "" {
		env = "local"
	}

	if len(os.Args) > 1 {
		env = os.Args[1]
	}

	viper := viper.New()
	viper.AddConfigPath(".")
	viper.SetConfigName("config." + env)
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	conf := &config.Default{}
	err = viper.Unmarshal(conf)
	if err != nil {
		panic(fmt.Errorf("unable to decode into struct, %v", err))
	}

	//setup log
	logger := SetupLogger(conf)

	//check db device
	if conf.DbDevice.Enable {
		NewDbDevice(conf, logger)
	}

	//check db
	if conf.Mysql.Enable {
		NewDbMySQL(conf, logger)
	}
	//check kafka
	if conf.Kafka.Enable {
		NewKafkaProducer(conf, logger)
	}

	// Listen for incoming connections.
	l, err := net.Listen("tcp", conf.Apps.Address+":"+conf.Apps.Port)
	if err != nil {
		fmt.Println("Error listening:", err.Error())
		os.Exit(1)
	}
	fmt.Println("tcp server running " + env + " port :" + conf.Apps.Port)
	// Close the listener when the application closes.
	defer l.Close()
	for {
		// Listen for an incoming connection.
		conn, err := l.Accept()
		if err != nil {
			fmt.Println("Error accepting: ", err.Error())
			os.Exit(1)
		}
		// Handle connections in a new goroutine.
		go handleRequest(conn, conf, logger)
	}
}

// Handles incoming requests.
func handleRequest(conn net.Conn, conf *config.Default, logger *Logger) {
	defer conn.Close()
	message := codec8.New()
	step := 1

	for {
		// Make a buffer to hold incoming data.
		buf := make([]byte, 2048)
		reqLen, err := conn.Read(buf)
		if err != nil {
			logger.syslog.Debug("handleRequest", zap.String("error", err.Error()))
			logger.syslog.Debug("handleRequest", zap.String("msg", "close connection.."))
			return
		}

		data := fmt.Sprintf("%x", buf[0:reqLen])
		logger.syslog.Debug("handleRequest", zap.String("msg", data))

		w := bufio.NewWriter(conn)
		if step == 1 {
			//auth & register imei
			w.Write([]byte{1})
			w.Flush()
			message.Imei = string(buf[3:reqLen])
			logger.syslog.Debug("handleRequest", zap.Any("imei", message.Imei))
			step++
			//validate device, get token
			token, err = ValidateDevice(conf, message.Imei, logger)
			if err != nil {
				logger.syslog.Debug("handleRequest", zap.String("msg", "validate imei error"))
				break
			}
			if token == "" {
				logger.syslog.Debug("handleRequest", zap.String("msg", "get token error"))
				break
			}
			//set tokenKey
			conf.HTTP.Body.TokenKey = token

		} else {
			if len(data) < 30 {
				logger.syslog.Debug("handleRequest", zap.String("msg", "invalid length"))
				break
			}

			//parse data
			err := message.Decode(data, &conf.Data)
			if err != nil {
				logger.syslog.Debug("handleRequest", zap.String("error", "parsing"+err.Error()))
				break
			}
			go handleRequestExtend(conf, message, logger)
			logger.syslog.Debug("handleRequest", zap.Any("response-NumberOfData", message.NumberOfData))

			w.Write([]byte{0, 0, 0, uint8(message.NumberOfData)})
			w.Flush()
			logger.tdr.Info("", zap.Any("message", message))
			break
		}

	}
}
func handleRequestExtend(conf *config.Default, message *codec8.Message, logger *Logger) {

	if conf.HTTP.Enable {
		fmt.Println("submit-http")
		SubmitHTTPCodec8(conf, message, logger)

	}
	if conf.Mysql.Enable {
		fmt.Println("submit-mysql")
		SubmitMySQLCodec8(conf, message, logger)
	}

	if conf.Kafka.Enable {
		fmt.Println("submit-kafka")
		SubmitKafkaCodec8(conf, message, logger)
	}

}
